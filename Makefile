include vars.mkf

xrnn : mkbin
	cd src && make
	cp src/XRNN_train bin/
	cp src/XRNN_test bin/
mkbin :
	mkdir -p bin

clean :
	cd src && make clean
	rm -rf bin

debug : mkbin
	cd src && make debug
	cp src/XRNN_train bin/
	cp src/XRNN_test bin/
