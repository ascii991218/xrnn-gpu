#include "XRnnLib.h"
#include "GLib.h"

int TPB(int x)
{
	if (x < 32) return 32;
	if (x < 64) return 64;
	if (x < 128) return 128;
	if (x < 256) return 256;
	if (x < 512) return 512;
	return 1024;
}

__global__
void setZeros(WEIGHT_TYPE *a, int size)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < size)
		a[i] = 0;
}

__device__
void warpReduce(volatile WEIGHT_TYPE *data, int tid)
{
	data[tid] += data[tid + 32];
	data[tid] += data[tid + 16];
	data[tid] += data[tid + 8];
	data[tid] += data[tid + 4];
	data[tid] += data[tid + 2];
	data[tid] += data[tid + 1];
}

__global__
void sum(WEIGHT_TYPE *a, int size, WEIGHT_TYPE* result)
{
	int tid = threadIdx.x;
	int i = threadIdx.x + blockIdx.x * 2 * blockDim.x;
	extern __shared__ WEIGHT_TYPE partial[];

	partial[tid] = (i < size) ? a[i] : 0;
	if (i + blockDim.x < size)
		partial[tid] += a[i + blockDim.x];
	__syncthreads();
	
	for (int step = blockDim.x / 2; step > 32; step >>= 1)
	{
		if (tid < step) partial[tid] += partial[tid + step];
		__syncthreads();
	}
	if (tid < 32) warpReduce(partial, tid);
	if (tid == 0) result[blockIdx.x] = partial[0];
}

__global__
void sigmoid(WEIGHT_TYPE *a, WEIGHT_TYPE *x, int size)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < size)
		a[i] = 1 / (1 + exp(-x[i]));
}

__global__
void softmax_exp(WEIGHT_TYPE *x, int size)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < size)
	{
		WEIGHT_TYPE t = exp(x[i]);
		if (isinf(t) || t > INF)
			t = INF;
		x[i] = t;
	}
}

__global__
void softmax_normalize(WEIGHT_TYPE *a, WEIGHT_TYPE *x, WEIGHT_TYPE sum, int size)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < size)
		a[i] = x[i] / sum;
}

void softmax(WEIGHT_TYPE *a, WEIGHT_TYPE *tmp, int size, cudaStream_t &stream)
{
	softmax_exp<<<BLOCK(size), TPB(size), 0, stream>>>(tmp, size);

	WEIGHT_TYPE *res, sum_res;
	
	int blockNum = (size + (MAX_TPB << 1) - 1) / (MAX_TPB << 1);
	cuda_safecall(cudaMalloc(&res, sizeof(WEIGHT_TYPE) * blockNum));
	sum<<<blockNum, MAX_TPB, MAX_TPB * sizeof(WEIGHT_TYPE), stream>>>(tmp, size, res);
	sum<<<1, MAX_TPB, MAX_TPB * sizeof(WEIGHT_TYPE), stream>>>(res, blockNum, res);

	cudaMemcpy(&sum_res, res, sizeof(WEIGHT_TYPE), cudaMemcpyDeviceToHost);
	if (sum_res < 0 || isinf(sum_res))
	{
		WEIGHT_TYPE *buf = new WEIGHT_TYPE[size];
		cudaMemcpy(buf, tmp, sizeof(WEIGHT_TYPE) * size, cudaMemcpyDeviceToHost);
		for (int i = 0; i < size; ++i)
			printf(WEIGHT_WRITE, buf[i]);
		printf("\n");
		gerr("Error! sum of probabilities is %.2f\n", sum_res);
	}
	softmax_normalize<<<BLOCK(size), TPB(size), 0, stream>>>(a, tmp, sum_res, size);

	cuda_safecall(cudaFree(res));
}

__global__
void sigerr(WEIGHT_TYPE *a, WEIGHT_TYPE *x, WEIGHT_TYPE *s, int size)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < size)
		a[i] = x[i] * s[i] * (1 - s[i]);
}

__global__
void softmaxerr(WEIGHT_TYPE *x, WEIGHT_TYPE *a, int target, int size)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < size)
	{
		a[i] = -x[i];
		if (i == target) a[i] += 1;
	}
}

__global__
void setupRandState(curandState *state, int size)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	int seed = i * i;
	if (i < size)
		curand_init(seed, 0, 0, &state[i]);
}

__global__
void genRand(curandState *state, WEIGHT_TYPE *mat, int len, int size)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < size)
	{
		curandState local = state[i];
		for (int t = 0; t < len; ++t)
			mat[size * t + i] = CURAND_UNIFORM(&local) / 5 - 0.1;
		state[i] = local;
	}
}

__global__ void weightConstrain(WEIGHT_TYPE *a, WEIGHT_TYPE m, int size)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < size)
	{
		if (a[i] > m)
			a[i] = m;
		else if (a[i] < -m)
			a[i] = -m;
	}
}

void weightConstrain(WEIGHT_TYPE *a, int size)
{
	weightConstrain<<<BLOCK(size), TPB(size)>>>(a, WEIGHT_MAX, size);
}

