#include "GLib.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "unistd.h" //For usleep function
#include <stdarg.h>
#include <time.h>
//#include "cuda_runtime.h"

const int MAX_WORD_LENGTH = 100;
const int VOCAB_HASH_SIZE = 100000000; //10^8
const double TOO_SMALL_PROB = 0.000000001; //1e-9
const int FEED_CONSECUTIVE_SENTENCE = 1000;
//const int VOCAB_HASH_SIZE = 1000000; //10^6
int debug_level; //Set in main() 
int debug_pause; //Set in main()
int warn_pause;

void glog(int log_level, const char *s, ...) {
    if (log_level <= debug_level) {
        va_list argptr;
        va_start(argptr, s);
        vfprintf(stdout, s, argptr);
		fflush(stdout);
        va_end(argptr);
		if (log_level >= 3)
			usleep(debug_pause);
    }
}

char* ggettime(char *buffer) {
	tm *temptm;  
	time_t temptime;  
	time(&temptime);
	temptm = localtime(&temptime);  
	sprintf(buffer, "%02d:%02d:%02d %d-%02d-%02d", temptm->tm_hour,temptm->tm_min,temptm->tm_sec, temptm->tm_year+1900,temptm->tm_mon+1,temptm->tm_mday);
	return buffer;
}
void gbiglog(int log_level, const char *s, ...) {
    if (log_level <= debug_level) {
		int band_length = 20;
		char buffer[200];
        va_list argptr;
        va_start(argptr, s);
        vsprintf(buffer, s, argptr);
		va_end(argptr);

		if (buffer[strlen(buffer) - 1] != '\n')
			strcat(buffer, "\n");

		if (strlen(buffer) + 5 > band_length)
			band_length = strlen(buffer) + 5;

		for (int i = 0;i < band_length;i++) printf("#"); printf("\n");
		printf("## "); printf("%s", buffer);
        for (int i = 0;i < band_length;i++) printf("#"); printf("\n");

    }
}
void gerr(const char *s, ...) {
    printf("[ERROR] : ");
    va_list argptr;
    va_start(argptr, s);
    vfprintf(stdout, s, argptr);
    va_end(argptr);
	fflush(stdout);
    exit(1);
}
void gwarn(const char *s, ...) {
    printf("[WARNING] : ");
    va_list argptr;
    va_start(argptr, s);
    vfprintf(stdout, s, argptr);
    va_end(argptr);
	if (warn_pause > 0)
		usleep(warn_pause);
}
float grand(float min, float max) {
    return rand()/(float)RAND_MAX*(max-min)+min;
}
/*
void gvprint(char *me, int n, WEIGHT_TYPE *a) {
	WEIGHT_TYPE *tmp = new WEIGHT_TYPE[n];
	cudaMemcpy(tmp, a, sizeof(WEIGHT_TYPE) * n, cudaMemcpyDeviceToHost);

	printf("%s ", me);
	for (int i = 0; i < n;i++)
		printf("%.7f ", tmp[i]);
	printf("\n");
	delete[] tmp;
	if (debug_pause > 0)
		usleep(debug_pause);
}*/
