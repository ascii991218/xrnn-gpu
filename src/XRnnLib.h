#ifndef __XRNNLIB_H
#define __XRNNLIB_H

#include <cstdio>
#include <cstring>
#include "cuda_runtime.h"
#include "cublas_v2.h"
#include "curand.h"
#include "curand_kernel.h"

#define MAX_TPB 1024

#define cuda_safecall(ans) { cudaAssert((ans), __FILE__, __LINE__); }
#define cublas_safecall(ans) { cublasAssert((ans), __FILE__, __LINE__); }

#define WEIGHT_TYPE_BE_FLOAT

#ifdef WEIGHT_TYPE_BE_FLOAT
	typedef float WEIGHT_TYPE;
	#define WEIGHT_WRITE "%.6f "
	#define WEIGHT_READ "%f"
	#define CUBLAS_GEMM cublasSgemm
	#define CUBLAS_AXPY cublasSaxpy
	#define CUBLAS_GEMV cublasSgemv
	#define CUBLAS_COPY cublasScopy
	#define CUBLAS_SCAL cublasSscal
	#define CURAND_UNIFORM curand_uniform
#endif

#ifdef WEIGHT_TYPE_BE_DOUBLE
	typedef double WEIGHT_TYPE;
	#define WEIGHT_WRITE "%.6lf "
	#define WEIGHT_READ "%lf"
	#define CUBLAS_GEMM cublasDgemm
	#define CUBLAS_AXPY cublasDaxpy
	#define CUBLAS_GEMV cublasDgemv
	#define CUBLAS_COPY cublasDcopy
	#define CUBLAS_SCAL cublasDscal
	#define CURAND_UNIFORM curand_uniform_double
#endif

const WEIGHT_TYPE WEIGHT_MAX = 15;
const double SMALL_PROB = 1e-10;
const WEIGHT_TYPE INF = 1e30;

inline void cudaAssert(cudaError_t msg, const char *file, int line)
{
	if (msg != cudaSuccess) 
	{
		printf("cuda call error at %s, %d: %s\n", file, line, cudaGetErrorString(msg));
		exit(msg);
	}
}

inline void cublasAssert(cublasStatus_t msg, const char *file, int line)
{
	if (msg != CUBLAS_STATUS_SUCCESS) 
	{
		printf("cublas call error at %s, %d\n", file, line);
		if (msg == CUBLAS_STATUS_ALLOC_FAILED)
			printf("reduction bufer cannot be allocated.\n");
		else if (msg == CUBLAS_STATUS_EXECUTION_FAILED)
			printf("failed to launch on the GPU.\n");
		else if (msg == CUBLAS_STATUS_NOT_INITIALIZED)
			printf("the library is not initialized.\n");
		else if (msg == CUBLAS_STATUS_ARCH_MISMATCH)
			printf("the device does not support double-precision.\n");
		exit(msg);
	}
}

int TPB(int x);

inline int BLOCK(int x)
{
	return (x + TPB(x) - 1) / TPB(x);
}

__global__ void setupRandState(curandState *state, int size);
__global__ void genRand(curandState *state, WEIGHT_TYPE *mat, int len, int size);
__global__ void setZeros(WEIGHT_TYPE *a, int size);
__device__ void warpReduce(volatile WEIGHT_TYPE *data, int tid);
__global__ void sum(WEIGHT_TYPE *a, int size, WEIGHT_TYPE* result);
__global__ void sigmoid(WEIGHT_TYPE *a, WEIGHT_TYPE *x, int size);
__global__ void softmax_exp(WEIGHT_TYPE *x, int size);
__global__ void softmax_normalize(WEIGHT_TYPE *a, WEIGHT_TYPE *x, WEIGHT_TYPE sum, int size);
__global__ void sigerr(WEIGHT_TYPE *a, WEIGHT_TYPE *x, WEIGHT_TYPE *s, int size);
__global__ void softmaxerr(WEIGHT_TYPE *x, WEIGHT_TYPE *a, int target, int size);
void weightConstrain(WEIGHT_TYPE *a, int size);
void softmax(WEIGHT_TYPE *a, WEIGHT_TYPE *tmp, int size, cudaStream_t &stream);

inline void vecAddVec(cublasHandle_t handle, WEIGHT_TYPE *x, const WEIGHT_TYPE *y, WEIGHT_TYPE alpha, int n)
{
	cublas_safecall(CUBLAS_AXPY(handle, n, &alpha, y, 1, x, 1));
}

inline void matMulVec(cublasHandle_t handle, const WEIGHT_TYPE *A, const WEIGHT_TYPE *x, WEIGHT_TYPE *y, int m, int n)
{
	WEIGHT_TYPE alpha = 1, beta = 0;
	cublas_safecall(CUBLAS_GEMV(handle, CUBLAS_OP_N, m, n, &alpha, A, m, x, 1, &beta, y, 1));
}

inline void matTransMulVec(cublasHandle_t handle, const WEIGHT_TYPE *A, const WEIGHT_TYPE *x, WEIGHT_TYPE *y, int m, int n)
{
	WEIGHT_TYPE alpha = 1, beta = 0;
	cublas_safecall(CUBLAS_GEMV(handle, CUBLAS_OP_T, m, n, &alpha, A, m, x, 1, &beta, y, 1));
}

inline void matMulMat(cublasHandle_t handle, const WEIGHT_TYPE *A, const WEIGHT_TYPE * B, WEIGHT_TYPE *C, int m, int n, int k)
{
	WEIGHT_TYPE t1 = 1, t2 = 0;
	cublas_safecall(CUBLAS_GEMM(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, k, &t1, A, m, B, k, &t2, C, m));
}

inline void matTransMulMat(cublasHandle_t handle, const WEIGHT_TYPE *A, const WEIGHT_TYPE * B, WEIGHT_TYPE *C, int m, int n, int k)
{
	WEIGHT_TYPE t1 = 1, t2 = 0;
	cublas_safecall(CUBLAS_GEMM(handle, CUBLAS_OP_T, CUBLAS_OP_N, m, n, k, &t1, A, k, B, k, &t2, C, m));
}

inline void scal(cublasHandle_t handle, WEIGHT_TYPE *a, WEIGHT_TYPE alpha, int size)
{
	cublas_safecall(CUBLAS_SCAL(handle, size, &alpha, a, 1));
}

inline void vecCopy(cublasHandle_t handle, WEIGHT_TYPE *dest, WEIGHT_TYPE *source, int size)
{
	cublas_safecall(CUBLAS_COPY(handle, size, source, 1, dest, 1));
}

class RandomInitializer
{
public:
	curandState *state;
	int size;

	RandomInitializer(int tsize)
	{
		size = tsize;
		cuda_safecall(cudaMalloc((void**) &state, sizeof(curandState) * size));
		setupRandState<<<BLOCK(size), TPB(size)>>>(state, size);
	}

	void set(WEIGHT_TYPE *a, int length)
	{
		genRand<<<BLOCK(size), TPB(size)>>>(state, a, length, size);
	}
};

#endif
