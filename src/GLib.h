#ifndef _GLIB_
#define _GLIB_

extern const int MAX_WORD_LENGTH;
extern const int VOCAB_HASH_SIZE;
extern const double TOO_SMALL_PROB;
extern const int FEED_CONSECUTIVE_SENTENCE;
extern int debug_level;
extern int debug_pause;
extern int warn_pause;
void glog(int log_level, const char *s, ...);
//void gvprint(char *me, int n, WEIGHT_TYPE *a); //First print the message, then the vector, ends with a line break.
char* ggettime(char *buffer); //Print the current time and date into buffer, returns the buffer
void gbiglog(int log_level, const char *s, ...); //Give a big, good display of the message
void gwarn(const char *s, ...);
void gerr(const char *s, ...); //Display the error message, and exit program
float grand(float min, float max);
#endif
