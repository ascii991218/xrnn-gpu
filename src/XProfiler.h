#include <ctime>
#include <cstdio>
#include <vector>
#include "cuda_runtime.h"

class XProfiler {
private:
	struct Event {
		char name[100];
		float last_time;
		float total_time;
		bool isGPU;
		cudaEvent_t start, stop;
		Event(char *aname, bool GPU = true)
		{
			strncpy(name, aname, 100);
			last_time = -1;
			total_time = 0;
			isGPU = GPU;
			if (isGPU) {
				cudaEventCreate(&start);
				cudaEventCreate(&stop);
			}
		}
	};
	std::vector<Event*> events;
	int n;
public:
	XProfiler();
	~XProfiler();
	void start(char *name, bool isGPU = true);
	void stop(char *name);
	void printToLog(); //Print using glog
};
