#include "GLib.h"
#include "XRnn.h"
#include "XRnnLib.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>    
#include <vector>

RandomInitializer *randGen;

int opPos(char* str, int argc, char** argv) 
{
    for (int i = 1;i < argc;i++)
        if (!strcmp(str, argv[i]))
            return i;
    return -1;
}

int main(int argc, char** argv)
{
    debug_level = 1; //will be reset
	char buffer[200];
    glog(1, "the X Recurrent Neural Network language model toolkit v0.2 : XRNN_train\n");
     
	GOpVec *ops = new GOpVec();
    ops->insert(new GOp("train_file", "file", "Set the training text file, we treat line feed as sentence delimiter, no <s> and </s> should appear. If no -read_vocab is set, the vocab will be constructed from this file.", NULL, false));
    ops->insert(new GOp("valid_file", "file", "Set the valid text file, same as train_file.", NULL, false));
	ops->insert(new GOp("read_lm_file", "file", "If set, xrnn(including gvocab) will be initialized by the file.", NULL, false));
	ops->insert(new GOp("output_lm_file", "file", "Model will be printed to output_lm_file after training.", NULL, false));
	ops->insert(new GOp("valid_first", "int", "Give a valid_file log probability in the first place.", "0"));
	ops->insert(new GOp("iter_number", "int", "The index of current(next) iteration.", "0"));
    ops->insert(new GOp("limit_vocab", "file", "Limit the vocab, otherwise vocab will be learned from the train_file.", NULL, false));
    ops->insert(new GOp("alpha", "float", "The learning rate, it will be normalized by bath_size during training.", "0.8"));
	ops->insert(new GOp("beta", "float", "The L2 regularization term.", "1e-6"));
    ops->insert(new GOp("rand_seed", "int", "The random seed.", "1"));
    ops->insert(new GOp("debug_level", "int", "Range from 1(most important), 2(running status), 3(diagnostic check added) 4(more info).", "2"));
	ops->insert(new GOp("debug_pause", "int", "Only for debug, pause for n micro-seconds at the end of each level >= 3 glog call and each gvprint call, about 100000 is okay.", "0"));
	ops->insert(new GOp("warn_pause", "int", "The microseconds paused for each gwarn call, about 100000 is okay.", "500000"));
	ops->insert(new GOp("independent", "int", "Whether to use independent sentence mode, 0 or 1, if set to 0, XNFeed will read batch_size * 10 consecutive sentences at one time.", "1"));
    ops->insert(new GOp("print_vocab", "file", "File to print the vocab.", NULL, false)); //could be NULL
    ops->insert(new GOp("hidden_size", "int", "Hidden layer size, which is also the word vector size.", "100"));
    ops->insert(new GOp("batch_size", "int", "Minibatch size(update after how many words).", "10"));
	ops->insert(new GOp("bptt", "int", "How many times to propagate the error back through time.", "3"));
	ops->insert(new GOp("gpu", "int", "which gpu device you want to use", "0"));
	ops->insert(new GOp("shuffle", "int", "shuffle the data if independent == 1", "0"));
	ops->insert(new GOp("min_improvement", "float", "stopping criteria", "1.003"));
	ops->insert(new GOp("min_iter", "int", "RNN will train at least min_iter iterations", "10"));
	//ops->insert(new GOp("weight_constrain", "int", "Constrain the weights.", "5"));
    std::vector<GOp*>::iterator it;

    if (argc == 1) {
        for (it = ops->begin(); it < ops->end(); it++)
            glog(1, "-%s <%s>\n  %s Default %s.\n", (*it)->name, (*it)->ty, (*it)->str, (*it)->def);
        return 0;
    }

    ops->sweepArgs(argc, argv);

	debug_level = atoi(ops->findOp("debug_level"));
	debug_pause = atoi(ops->findOp("debug_pause"));
	warn_pause = atoi(ops->findOp("warn_pause"));

    for (it = ops->begin(); it < ops->end(); it++)
        glog(1, "-%s=%s %s\n", (*it)->name, (*it)->get, (*it)->how);
	glog(1, "-FEED_CONSECUTIVE_SENTENCE=%d (pre-set-constants)\n", FEED_CONSECUTIVE_SENTENCE);

    srand(atoi(ops->findOp("rand_seed")));
	cuda_safecall(cudaSetDevice(atoi(ops->findOp("gpu"))));
	randGen = new RandomInitializer(atoi(ops->findOp("hidden_size")));

    GVocab *gvocab = new GVocab(atoi(ops->findOp("hidden_size")));

    XRNN *xrnn = new XRNN(ops, gvocab);

	if (ops->findOp("read_lm_file") != NULL)
		xrnn->initializeByFile();
	else
	{
		if (ops->findOp("limit_vocab") != NULL)
			gvocab->loadVocabFile(ops->findOp("limit_vocab"));
		else
			gvocab->learnVocabFromFile(ops->findOp("train_file"));
		xrnn->initialize();
	}

    if (ops->findOp("print_vocab") != NULL) 
       gvocab->printVocab(ops->findOp("print_vocab"));

	if (atoi(ops->findOp("valid_first")) == 1)
	{
		gbiglog(1, "INITIAL CROSSVAL %s", ggettime(buffer));
		xrnn->testOnFile(ops->findOp("valid_file"));	
		gbiglog(1, "INITIAL CROSSVAL ENDS %s", ggettime(buffer));
	}

	if (ops->findOp("train_file") != NULL)
		xrnn->train();

	if (ops->findOp("output_lm_file") != NULL)
		xrnn->printModel();

	delete gvocab;
	delete xrnn;
	delete ops;
	
	return 0;
}
