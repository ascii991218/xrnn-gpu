#include "XRnn.h"
#include "XRnnLib.h"

#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <fcntl.h>

TestRes::TestRes(double d1, double d2, int wc, int sc, int oc) 
{
	logp_net = d1; logp_net_oovpart = d2;
	logp_other = 0; logp_other_oovpart = 0;
	logp_combine = 0; logp_combine_oovpart = 0;
	wordcn = wc; sencn = sc; oovcn = oc;
}

TestRes::TestRes(double d1, double d2, double d3, double d4, double d5, double d6, int wc, int sc, int oc) 
{
	logp_net = d1; logp_net_oovpart = d2;
	logp_other = d3; logp_other_oovpart = d4;
	logp_combine = d5; logp_combine_oovpart = d6;
	wordcn = wc; sencn = sc; oovcn = oc;
}

double TestRes::giveNetPPL() 
{
	return exp((-(logp_net - logp_net_oovpart)) / (wordcn - oovcn + sencn));
}

double TestRes::giveNetPPLwithOOV() 
{
	return exp(-(logp_net) / (wordcn + sencn));
}

double TestRes::giveOtherPPL() 
{
	return exp((-(logp_other - logp_other_oovpart)) / (wordcn - oovcn + sencn));
}

double TestRes::giveOtherPPLwithOOV() 
{
	return exp(-(logp_other) / (wordcn + sencn));
}

double TestRes::giveCombinePPL() 
{
	return exp((-(logp_combine - logp_combine_oovpart)) / (wordcn - oovcn + sencn));
}

double TestRes::giveCombinePPLwithOOV() 
{
	return exp(-(logp_combine) / (wordcn + sencn));
}

XRNN::XRNN(GOpVec *operands, GVocab *voc)
{
	batch_size = atoi(operands->findOp("batch_size"));
	hidden_size = atoi(operands->findOp("hidden_size"));
	bptt = atoi(operands->findOp("bptt"));
	alpha = atof(operands->findOp("alpha"));
	beta = atof(operands->findOp("beta"));
	independent = atoi(operands->findOp("independent"));
	vocab = voc;
	output_size = vocab->size();
	opVec = operands;
	profiler = new XProfiler();
	
	initialized = false;
	senEnd = vocab->findWord("</s>");
	unkId = vocab->findWord("<unk>");
}

XRNN::~XRNN()
{
	cuda_safecall(cudaFree(cuda_weightHH));
	cuda_safecall(cudaFree(cuda_weightHO));
	cuda_safecall(cudaFree(cuda_y));
	cuda_safecall(cudaFree(cuda_history_s));
	cuda_safecall(cudaFree(cuda_history_e_h));
	cuda_safecall(cudaFree(cuda_tmp));
	delete[] batchSt;
	delete[] batchHis;
	delete profiler;
	
	for (int i = 0; i < batch_size; ++i)
	{
		cuda_safecall(cudaStreamDestroy(stream[i]));
		cublas_safecall(cublasDestroy(handles[i]));
	}
	delete[] stream;
	delete[] handles;
	cublas_safecall(cublasDestroy(handle0));
}

void XRNN::makeSpace()
{
	max_size = hidden_size;
	output_size = vocab->size(); // This is important, GVocab have already read words.
	if (output_size > max_size) max_size = output_size;
	cuda_safecall(cudaMalloc((void**) &cuda_weightHH, hidden_size * hidden_size * sizeof(WEIGHT_TYPE)));
	cuda_safecall(cudaMalloc((void**) &cuda_weightHO, hidden_size * output_size * sizeof(WEIGHT_TYPE)));
	cuda_safecall(cudaMalloc((void**) &cuda_y, output_size * batch_size * sizeof(WEIGHT_TYPE)));
	cuda_safecall(cudaMalloc((void**) &cuda_history_s, (bptt + 2) * hidden_size * batch_size * sizeof(WEIGHT_TYPE)));
	cuda_safecall(cudaMalloc((void**) &cuda_history_e_h, (bptt + 2) * hidden_size * batch_size * sizeof(WEIGHT_TYPE)));
	cuda_safecall(cudaMalloc((void**) &cuda_tmp, max_size * batch_size * sizeof(WEIGHT_TYPE)));

	batchHis = new int[batch_size * (bptt + 2)];
	batchSt = new int[batch_size];
	
	stream = new cudaStream_t[batch_size];
	handles = new cublasHandle_t[batch_size];

	cublas_safecall(cublasCreate(&handle0));
	for (int i = 0; i < batch_size; ++i)
	{
		cuda_safecall(cudaStreamCreate(&stream[i]));
		cublas_safecall(cublasCreate(&handles[i]));
		cublas_safecall(cublasSetStream(handles[i], stream[i]));
	}
}

void XRNN::initialize()
{
	glog(1, "start initializing\n");
	makeSpace();
	randGen->set(cuda_weightHH, hidden_size);
	randGen->set(cuda_weightHO, output_size);
	initialized = true;
	glog(1, "finished initializing\n");
}

void XRNN::initializeByFile()
{
	glog(1, "start initializing, reading model file.\n");
	FILE *file = fopen(opVec->findOp("read_lm_file"), "r");
	vocab->getWVFromFile(file);
	makeSpace();
	
	WEIGHT_TYPE *tmp = new WEIGHT_TYPE[hidden_size * max_size];
	int a, b;
	char buf[50];

	fscanf(file, "%s %d %d", buf, &a, &b);
	if (a != hidden_size || b != hidden_size)
		gerr("XRnn::initializeByFile weightHH(%d %d) size mismatch.\n", a, b);
	for (int i = 0; i < hidden_size * hidden_size; ++i)
			fscanf(file, WEIGHT_READ, tmp + i);
	cuda_safecall(cudaMemcpy(cuda_weightHH, tmp, sizeof(WEIGHT_TYPE) * hidden_size * hidden_size, cudaMemcpyHostToDevice));

	fscanf(file, "%s %d %d", buf, &a, &b);
	if (a != hidden_size || b != output_size)
		gerr("XRnn::initializeByFile weightHO(%d %d) size mismatch.\n", a, b);
	for (int i = 0; i < hidden_size * output_size; ++i)
			fscanf(file, WEIGHT_READ, tmp + i);
	cuda_safecall(cudaMemcpy(cuda_weightHO, tmp, sizeof(WEIGHT_TYPE) * hidden_size * output_size, cudaMemcpyHostToDevice));

	fclose(file);
	delete[] tmp;
	initialized = true;
	glog(1, "finished reading lm from file.\n");
}

void XRNN::printModel()
{
	if (opVec->findOp("output_lm_file") == NULL)
		gerr("XRnn::printModel output_lm_file is NULL.");
	glog(1, "XRnn::printModel outputing model to %s...\n", opVec->findOp("output_lm_file"));

	FILE *file = fopen(opVec->findOp("output_lm_file"), "w");

	WEIGHT_TYPE *tmp = new WEIGHT_TYPE[hidden_size * max_size];

	fprintf(file, "VOCAB %d %d\n", vocab->size(), vocab->getVecSize());
	for (int i = 0; i < vocab->size(); ++i) 
	{
		fprintf(file, "%s ", vocab->getWord(i)->word);
		cuda_safecall(cudaMemcpy(tmp, vocab->getWord(i)->vec, sizeof(WEIGHT_TYPE) * hidden_size, cudaMemcpyDeviceToHost));
		for (int j = 0; j < vocab->getVecSize(); ++j)
			fprintf(file, WEIGHT_WRITE, tmp[j]);
		fprintf(file, "\n");
	}

	cuda_safecall(cudaMemcpy(tmp, cuda_weightHH, sizeof(WEIGHT_TYPE) * hidden_size * hidden_size, cudaMemcpyDeviceToHost));
	fprintf(file, "WEIGHTHH %d %d\n", hidden_size, hidden_size);
	for (int i = 0; i < hidden_size * hidden_size; ++i)
		fprintf(file, WEIGHT_WRITE, tmp[i]);
	fprintf(file, "\n");

	cuda_safecall(cudaMemcpy(tmp, cuda_weightHO, sizeof(WEIGHT_TYPE) * hidden_size * output_size, cudaMemcpyDeviceToHost));
	fprintf(file, "WEIGHTHO %d %d\n", hidden_size, output_size);
	for (int i = 0; i < hidden_size * output_size; ++i)
		fprintf(file, WEIGHT_WRITE, tmp[i]);
	fprintf(file, "\n");

	delete[] tmp;
	fclose(file);	
	glog(1, "XRnn::printModel outputing complete.\n");
}

void XRNN::restore()
{
	glog(1, "restoring from previous iteration.\n");
	FILE *file = fopen(opVec->findOp("output_lm_file"), "r");
	vocab->getWVFromFile(file);
	makeSpace();
	
	WEIGHT_TYPE *tmp = new WEIGHT_TYPE[hidden_size * max_size];
	int a, b;
	char buf[50];

	fscanf(file, "%s %d %d", buf, &a, &b);
	for (int i = 0; i < hidden_size * hidden_size; ++i)
			fscanf(file, WEIGHT_READ, tmp + i);
	cuda_safecall(cudaMemcpy(cuda_weightHH, tmp, sizeof(WEIGHT_TYPE) * hidden_size * hidden_size, cudaMemcpyHostToDevice));

	fscanf(file, "%s %d %d", buf, &a, &b);
	for (int i = 0; i < hidden_size * output_size; ++i)
			fscanf(file, WEIGHT_READ, tmp + i);
	cuda_safecall(cudaMemcpy(cuda_weightHO, tmp, sizeof(WEIGHT_TYPE) * hidden_size * output_size, cudaMemcpyHostToDevice));

	fclose(file);
	delete[] tmp;
	glog(1, "finished restoring to previous iteration.\n");
}

bool shuffle(const char *fn)
{
	char buffer[200];
	strncpy(buffer, fn, 200);
	int pid = fork();
	if (pid < 0) return false;
	else if (pid == 0)
	{
		int input = open(fn, O_RDONLY);
		dup2(input, STDIN_FILENO);
		char * args[4] = {"shuf", "-o", buffer, NULL};
		execvp(args[0], args);
	}
	else
	{
		int status;
		wait(&status);
		if (WIFEXITED(status))
			return true;
		else
			return false;
	}
	return false;
}

void XRNN::train()
{
	double nowProb, prevProb, ppl, improve;
	bool stop = false, alpha_decay = false;
	int min_iter = atoi(opVec->findOp("min_iter"));
	int iter_st = atoi(opVec->findOp("iter_number"));
	double min_improvement = atof(opVec->findOp("min_improvement"));
	WEIGHT_TYPE tbeta = beta;
	int iter = iter_st; 
	TestRes *result;

	if (opVec->findOp("read_lm_file") != NULL)
	{
		result = testOnFile(opVec->findOp("valid_file"));
		nowProb = result->logp_net;
		ppl = result->giveNetPPLwithOOV();
		glog(1, "iteration %d: alpha: %.6f, perplexity %.2f\n", iter, alpha, ppl);
		delete result;
		++iter;
	}		

	if (initialized == false)
		gerr("XRnn not initialized!\n");
	glog(1, "start training\n");
	while (1)
	{
		beta = tbeta * alpha;
		if (iter >= min_iter && stop)
			break;
		if (atoi(opVec->findOp("shuffle")) == 1)
		{
			if (shuffle(opVec->findOp("train_file")) == false)
				gerr("shuffle failed!.\n");
			else
				glog(1, "shuffle success.\n");
		}
		profiler->start("training", false);
		trainOneIter(iter);
		WEIGHT_TYPE tmp = prevProb;
		prevProb = nowProb;
		result = testOnFile(opVec->findOp("valid_file"));
		profiler->stop("training");
		profiler->printToLog();
		nowProb = result->logp_net;
		ppl = result->giveNetPPLwithOOV();
		glog(1, "iteration %d: alpha: %.6f, perplexity %.2f\n", iter, alpha, ppl);
		delete result;
		
		improve = prevProb / nowProb;
		if (iter > iter_st && improve < min_improvement)
		{
			if (alpha_decay == false) alpha_decay = true;
			else stop = true;
		}
		if (iter > iter_st) glog(1, "improvement %.6f\n", improve);
		if (alpha_decay) alpha *= 0.5;
		if (iter == iter_st || improve > 1) printModel();
		else {
			restore();
			prevProb = tmp;
			if (iter < min_iter - 1) --iter;
		}
		++iter;
	}
	glog(1, "training finished.\n");
}

void XRNN::trainOneIter(int iter)
{
	feeder = new XNFeed(batch_size, independent, vocab);
	feeder->initialize(opVec->findOp("train_file"));
	feeder->nextBatch(batchHis);
	memset(batchSt, 0, sizeof(int) * batch_size);
	for (int step = 0; ; ++step)
	{
		int idx = step % (bptt + 2);
		int nxt = (step + 1) % (bptt + 2);
		batch = batchHis + idx * batch_size;
		target = batchHis + nxt * batch_size;
		if (feeder->nextBatch(target) == false)
			break;
		forward(step);
		backPropagate(step);
		if (step > 0 && step % 1000 == 0) glog(1, "+");
	}
	glog(1, "\n");
	delete feeder;
}

void printBuffer(FILE *file, WEIGHT_TYPE *buf, int hidden_size, int sum)
{
	for (int i = 0; i < sum; ++i)
	{
		for (int j = 0; j < hidden_size; ++j)
			if (abs(buf[i * hidden_size + j]) > 5e-7)
			{
				//fprintf(file, "%d:", i); for libsvm format
				fprintf(file, WEIGHT_WRITE, buf[i * hidden_size + j]);
			}
			else // delete this for libsvm format
				fprintf(file, "0 ");
		fprintf(file, "\n");
	}
}

double XRNN::printSentence(FILE *fp, FILE *fv)
{
	feeder = new XNFeed(batch_size, independent, vocab);
	feeder->initialize(opVec->findOp("ppl_file"));
	feeder->nextBatch(batchHis);

	const int BUFFER_SENTENCE = 10000;
	memset(batchSt, 0, sizeof(int) * batch_size);
	WEIGHT_TYPE *bufp = new WEIGHT_TYPE[hidden_size * BUFFER_SENTENCE];
	WEIGHT_TYPE *bufv = new WEIGHT_TYPE[hidden_size * BUFFER_SENTENCE];
	WEIGHT_TYPE tmp;
	double logProb = 0, totalProb = 0, ans;
	int wordCnt = 0, totalCnt = 0, sidp = 0, sidv = 0;
	for (int step = 0; ; ++step)
	{
		int idx = step % (bptt + 2);
		int nxt = (step + 1) % (bptt + 2);
		batch = batchHis + idx * batch_size;
		target = batchHis + nxt * batch_size;
		if (feeder->nextBatch(target) == false)
			break;
		forward(step);
		if (target[0] >= 0)
		{
			cudaMemcpy(&tmp, &cuda_y[target[0]], sizeof(WEIGHT_TYPE), cudaMemcpyDeviceToHost);
			if (tmp < SMALL_PROB)
				ans = SMALL_PROB;
			else
				ans = tmp;
			logProb += log(ans);
			totalProb += log(ans);
			++wordCnt;
			++totalCnt;
		}
		if (target[0] == senEnd)
		{
			if (fp != NULL)
			{
				bufp[sidp++] = logProb / wordCnt;
				logProb = wordCnt = 0;
				if (sidp == BUFFER_SENTENCE * hidden_size)
				{
					printBuffer(fp, bufp, 1, sidp);
					sidp = 0;
				}
			}
			if (fv != NULL)
			{
				WEIGHT_TYPE *s = cuda_history_s + idx * batch_size * hidden_size; // remember that batch_size == 1
				cuda_safecall(cudaMemcpy(bufv + sidv * hidden_size, s, sizeof(WEIGHT_TYPE) * hidden_size, cudaMemcpyDeviceToHost));
				++sidv;
				if (sidv== BUFFER_SENTENCE)
				{
					printBuffer(fv, bufv, hidden_size, sidv);
					sidv = 0;
				}
			}
		}
	}
	if (fp != NULL && sidp > 0)
		printBuffer(fp, bufp, 1, sidp);
	if (fv != NULL && sidv > 0)
		printBuffer(fv, bufv, hidden_size, sidv);
	delete[] bufv;
	delete[] bufp;
	return totalProb / totalCnt;
}

TestRes* XRNN::testOnFile(const char *fn)
{
	double logProb = 0, logoovProb = 0;
	int wordCnt = 0, oovCnt = 0, batchBak;
	XNFeed *tmpFeeder = feeder;
	
	glog(1, "start testing\n");
	if (independent == 0) 
	{
		batchBak = batch_size;
		batch_size = 1;
	}
	feeder = new XNFeed(batch_size, independent, vocab);
	feeder->initialize(fn);
	feeder->nextBatch(batchHis);
	memset(batchSt, 0, sizeof(int) * batch_size);
	for (int step = 0; ; ++step)
	{
		int idx = step % (bptt + 2);
		int nxt = (step + 1) % (bptt + 2);
		batch = batchHis + idx * batch_size;
		target = batchHis + nxt * batch_size;
		if (feeder->nextBatch(target) == false)
			break;
		forward(step);
		for (int i = 0; i < batch_size; ++i)
		{
			WEIGHT_TYPE tmp;
			double ans;
			if (batch[i] != -1 && target[i] != -1)
			{
				cudaMemcpy(&tmp, &cuda_y[i * output_size + target[i]], sizeof(WEIGHT_TYPE), cudaMemcpyDeviceToHost);
				if (tmp < SMALL_PROB)
					ans = SMALL_PROB;
				else
					ans = tmp;
				logProb += log(ans);
				++wordCnt;
				if (target[i] == unkId) 
				{
					++oovCnt;
					logoovProb += log(ans);
				}
			}
		}
		if (step > 0 && step % 1000 == 0) glog(1, "+");
	}
	glog(1, "\ntesting finished, wordCnt=%d, oovCnt=%d\n", wordCnt, oovCnt);

	if (independent == 0) batch_size = batchBak;
	delete feeder;
	feeder = tmpFeeder;
	return new TestRes(logProb, logoovProb, wordCnt, 0, oovCnt);
}

void XRNN::forward(int step)
{
	int idx = step % (bptt + 2);
	int id = idx - 1;
	if (id < 0) id = bptt + 1;
	WEIGHT_TYPE *per_s = cuda_history_s + id * batch_size * hidden_size;
	WEIGHT_TYPE *s = cuda_history_s + idx * batch_size * hidden_size;

	for (int i = 0; i < batch_size; ++i)
		if (independent == 1 && batch[i] == senEnd) 
			batchSt[i] = step;
		else if (batch[i] == -1)
			batchSt[i] = step + 1;
	if (debug_level >= 3) profiler->start("weightHH * per_hidden");
	matTransMulMat(handle0, cuda_weightHH, per_s, cuda_tmp, hidden_size, batch_size, hidden_size);
	if (debug_level >= 3) profiler->stop("weightHH * per_hidden");
	if (debug_level >= 3) profiler->start("sigmoid activation");
	for (int i = 0; i < batch_size; ++i)
		//if (independent && batch[i] == senEnd)
		if (batchSt[i] == step)
			setZeros<<<BLOCK(hidden_size), TPB(hidden_size), 0, stream[i]>>>(cuda_tmp + hidden_size * i, hidden_size);
	for (int i = 0; i < batch_size; ++i)
		if (batch[i] != -1)
			vecAddVec(handles[i], cuda_tmp + hidden_size * i, vocab->getWord(batch[i])->vec, 1, hidden_size);
	sigmoid<<<BLOCK(hidden_size * batch_size), TPB(hidden_size * batch_size)>>>(s, cuda_tmp, hidden_size * batch_size);
	if (debug_level >= 3) profiler->stop("sigmoid activation");
	if (debug_level >= 3) profiler->start("weightHO * cur_hidden");
	matTransMulMat(handle0, cuda_weightHO, s, cuda_tmp, output_size, batch_size, hidden_size);
	if (debug_level >= 3) profiler->stop("weightHO * cur_hidden");
	if (debug_level >= 3) profiler->start("softmax activation");
	for (int i = 0; i < batch_size; ++i)
		if (batch[i] != -1 && target[i] != -1)
			softmax(cuda_y + output_size * i, cuda_tmp + output_size * i, output_size, stream[i]);
	if (debug_level >= 3) profiler->stop("softmax activation");
	cuda_safecall(cudaDeviceSynchronize());
}

void XRNN::backPropagate(int step)
{
	int idx = step % (bptt + 2);
	if (debug_level >= 3) profiler->start("softmax error calculation");
	for (int i = 0; i < batch_size; ++i)
		if (batch[i] == -1 || target[i] == -1)
			setZeros<<<BLOCK(output_size), TPB(output_size), 0, stream[i]>>>(cuda_y + output_size * i, output_size);
	scal(handle0, cuda_y, -1, output_size * batch_size);

	for (int i = 0; i < batch_size; ++i)
		if (batch[i] != -1 && target[i] != -1)
		{
			WEIGHT_TYPE tmp;
			cudaMemcpy(&tmp, &cuda_y[i * output_size + target[i]], sizeof(WEIGHT_TYPE), cudaMemcpyDeviceToHost);
			tmp += 1;
			cudaMemcpy(&cuda_y[i * output_size + target[i]], &tmp, sizeof(WEIGHT_TYPE), cudaMemcpyHostToDevice);
		}
	if (debug_level >= 3) profiler->stop("softmax error calculation");
	if (debug_level >= 3) profiler->start("softmax error propagation");
	matMulMat(handle0, cuda_weightHO, cuda_y, cuda_tmp, hidden_size, batch_size, output_size);
	if (debug_level >= 3) profiler->stop("softmax error propagation");

	if (debug_level >= 3) profiler->start("sigmoid error calculation");
	WEIGHT_TYPE *e_h = cuda_history_e_h + idx * hidden_size * batch_size;
	WEIGHT_TYPE *s = cuda_history_s + idx * hidden_size * batch_size;
	sigerr<<<BLOCK(hidden_size * batch_size), TPB(hidden_size * batch_size)>>>(e_h, cuda_tmp, s, hidden_size * batch_size);
	weightConstrain(e_h, hidden_size * batch_size);
	if (debug_level >= 3) profiler->stop("sigmoid error calculation");

	if (debug_level >= 3) profiler->start("bptt update word vector");
	for (int t = 0, id = idx; t <= bptt && step - t >= 0; ++t)
	{
		for (int i = 0; i < batch_size; ++i)
			if (step - t >= batchSt[i] && target[i] != -1)
			{
				scal(handles[i], vocab->getWord(batchHis[id * batch_size + i])->vec, 1 - beta, hidden_size);
				vecAddVec(handles[i], vocab->getWord(batchHis[id * batch_size + i])->vec, e_h + hidden_size * i, alpha, hidden_size);
			}

		for (int i = 0; i < batch_size; ++i)
			if (step - t == batchSt[i])
				setZeros<<<BLOCK(hidden_size), TPB(hidden_size), 0, stream[i]>>>(e_h + hidden_size * i, hidden_size);
		if (t == bptt || step - t == 0) break;

		matMulMat(handle0, cuda_weightHH, e_h, cuda_tmp, hidden_size, batch_size, hidden_size);
		--id;
		if (id < 0) id = bptt + 1;
		s = cuda_history_s + id * hidden_size * batch_size;
		e_h = cuda_history_e_h + id * hidden_size * batch_size;
		sigerr<<<BLOCK(hidden_size * batch_size), TPB(hidden_size * batch_size)>>>(e_h, cuda_tmp, s, hidden_size * batch_size);
		weightConstrain(e_h, hidden_size * batch_size);
	}
	if (debug_level >= 3) profiler->stop("bptt update word vector");
	if (debug_level >= 3) profiler->start("update weightHO");
	WEIGHT_TYPE t1 = 1 - beta;
	s = cuda_history_s + idx * hidden_size * batch_size;
	cublas_safecall(CUBLAS_GEMM(handle0, CUBLAS_OP_N, CUBLAS_OP_T, hidden_size, output_size, batch_size, &alpha, s, hidden_size, cuda_y, output_size, &t1, cuda_weightHO, hidden_size));
	if (debug_level >= 3) profiler->stop("update weightHO");

	if (debug_level >= 3) profiler->start("bptt update weightHH");
	scal(handle0, cuda_weightHH, 1 - beta, hidden_size * hidden_size);
	t1 = 1; 
	for (int t = 0, id = idx; t <= bptt && step - t > 0; ++t)
	{
		e_h = cuda_history_e_h + id * hidden_size * batch_size;
		--id;
		if (id < 0) id = bptt + 1;
		s = cuda_history_s + id * hidden_size * batch_size;
		cublas_safecall(CUBLAS_GEMM(handle0, CUBLAS_OP_N, CUBLAS_OP_T, hidden_size, hidden_size, batch_size, &alpha, s, hidden_size, e_h, hidden_size, &t1, cuda_weightHH, hidden_size));
	}
	if (debug_level >= 3) profiler->stop("bptt update weightHH");
	weightConstrain(cuda_weightHH, hidden_size * hidden_size);
	weightConstrain(cuda_weightHO, hidden_size * output_size);
}
