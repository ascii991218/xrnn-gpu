#ifndef __XRNN_H
#define __XRNN_H

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include "GVocab.h"
#include "GOpVec.h"
#include "XNFeed.h"
#include "XProfiler.h"
#include "cuda_runtime.h"
#include "cublas_v2.h"

//only for debugging purpose, disables all asynchronous behaviour
//#define CUDA_LAUNCH_BLOCKING 1
extern RandomInitializer *randGen;
class TestRes 
{
	public:
		double logp_net;
		double logp_net_oovpart;

		double logp_other;
		double logp_other_oovpart;
		
		double logp_combine;
		double logp_combine_oovpart;

		int wordcn, sencn, oovcn;
		int independent;

		TestRes(double d1, double d2, int wc, int sc, int oc);
		TestRes(double d1, double d2, double d3, double d4, double d5, double d6, int wc, int sc, int oc);
		double giveNetPPL();
		double giveNetPPLwithOOV();
		double giveOtherPPL();
		double giveOtherPPLwithOOV();
		double giveCombinePPL();
		double giveCombinePPLwithOOV();
};

class XRNN
{
public:
	WEIGHT_TYPE *cuda_weightHH; //Recurrent matrix Hidden->Hidden, size: hidden * hidden
	WEIGHT_TYPE *cuda_weightHO; //Output matrix Hidden->Output, size: hidden * output
	WEIGHT_TYPE *cuda_y; //Output layer activation and error, size: output * batch
	WEIGHT_TYPE *cuda_tmp; //Temporary for saving intermediate results, size: max(hidden, output)* batch
	WEIGHT_TYPE *cuda_history_s; //History of hidden activation, size: (bptt + 2) * hidden * batch
	WEIGHT_TYPE *cuda_history_e_h; //History of hidden error, size: hidden * batch

	int *batchSt; //batchSt[i] is the first word of the batch, it is either the last position of </s>(independent case) or the successor of NULL(continuous case)
	int *batch; //batch points to the current batch
	int *target; //target points to the next batch
	int *batchHis; //batchHistory, size: (bptt + 2) * batch
	
	int batch_size;
	int hidden_size;
	int max_size;
	int output_size;
	int bptt;
	int independent;
	WEIGHT_TYPE alpha, beta;

	bool initialized;
	int senEnd; // denotes </s>
	int unkId; // denotes <unk>

	GOpVec *opVec;
	XNFeed *feeder;
	GVocab *vocab;
	XProfiler *profiler;
	cudaStream_t *stream; // each stream deals with a batch
	cublasHandle_t *handles, handle0; // cublas library context, each handle deals with a batch
public:
	XRNN(GOpVec*, GVocab*);
	~XRNN();

	void makeSpace();
	void initializeByFile();
	void initialize();
	void printModel();
	void restore();

	void train();
	void trainOneIter(int);
	TestRes* testOnFile(const char *);
	void forward(int);
	void backPropagate(int);
	double printSentence(FILE *, FILE *);
};
#endif
