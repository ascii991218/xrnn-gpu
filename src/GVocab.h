#ifndef _GVOCAB_H_
#define _GVOCAB_H_

#include "GLib.h"
#include <stdio.h>
#include <vector>
#include "XRnnLib.h"

extern const int MAX_WORD_LENGTH;
extern RandomInitializer *randGen;

class GWord {
    public:
        char *word;
        WEIGHT_TYPE *vec;
        GWord(const char *w, int s);
	~GWord();
};

class GVocab { //</s> and <unk> will be in vocab when it's constructed.
    private:
        int vec_size;
        std::vector<GWord*> *words;
        void rehash(); //rearrange the hash_table according to the words in the vector
        int *hash_table; //The hash table would contain all words, it need to be re-arranged if changes are made to the vocab.
        void addWordToHash(const char *w, int index);
        int addWord(const char *w); //Return the index of the word, if the word is added, the word's vector will also be initialized
		int addWV(const char *w, const WEIGHT_TYPE *v); //Add the word along with the vector, if the word exists, its vector will be over-writed.
    public:
        int getHash(const char *w); //Return the hash value of a word, already mod by VOCAB_HASH_SIZE
		GWord* getWord(int index);
        int findWord(const char *w); //Find a word(remember <unk> is also in vocab) according to the hash_table, important, if not found, return -1
        void readWord(char* word, FILE *f); //Read a word from file, \n is return as </s>, <unk> is returned as <unk>, user should provider a buffer
        void loadVocabFile(const char *f); //Add all words in file plus </s> to vocab
        int learnVocabFromFile(const char *f); //Return how many words are learned, the file will be fopened and fclosed
		int getWVFromFile(FILE *file); //Read Vocab from an ALREADY-FOPENED file, return the resulting vocab size
        int size();
        int getVecSize();
        GVocab(int vs);
	~GVocab();
        void printVocab(const char *f);
		int getUnkInd();
};

#endif
