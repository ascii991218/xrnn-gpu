#include "XProfiler.h"
#include "GLib.h"
#include <string.h>

using std::vector;

XProfiler::XProfiler() {
	events = vector<Event*>();
	n = 0;
}
XProfiler::~XProfiler()
{
	for (int i = 0; i < n; ++i)
		delete events[i];
}

void XProfiler::start(char *name, bool isGPU) {
	int k = 0;
	for (k = 0;k < n; k++)
		if (strcmp(events[k]->name, name) == 0)
			break;
	if (k == n) {
		Event *e = new Event(name, isGPU);
		events.push_back(e);
		n++;
	}
	if (events[k]->last_time >= 0)
		gerr("XProfiler::start event %s restarted.\n", events[k]->name);
	if (events[k]->isGPU != isGPU)
		gerr("XProfiler::cannot switch event %s between CPU and GPU.", events[k]->name); 
	if (events[k]->isGPU)
		cudaEventRecord(events[k]->start, 0);
	events[k]->last_time = std::clock();
}

void XProfiler::stop(char *name) {
	int k = 0;
	for (k = 0;k < n;k++) {
		if (strcmp(events[k]->name, name) == 0)
			break;
	}
	if (k == n)
		gerr("XProfiler::stop event %s not found.\n", name);
	if (events[k]->last_time < 0)
		gerr("XProfiler::stop event %s not started.\n", name);
	if (events[k]->isGPU == false)
		events[k]->total_time += (std::clock() - events[k]->last_time) / (float)CLOCKS_PER_SEC;
	else
	{
		cudaEventRecord(events[k]->stop, 0);
		cudaEventSynchronize(events[k]->stop);
		float elapsed_time;
		cudaEventElapsedTime(&elapsed_time, events[k]->start, events[k]->stop);
		events[k]->total_time += elapsed_time;
	}
	events[k]->last_time = -1;
}

void XProfiler::printToLog() {
	for (int i = 0;i < n;i++)
		if (events[i]->isGPU)
			glog(3, "XProfiler::printToLog event %s runs for %lf seconds.\n", events[i]->name, events[i]->total_time / 1000);
		else
			glog(3, "XProfiler::printToLog event %s runs for %lf seconds.\n", events[i]->name, events[i]->total_time);

}
