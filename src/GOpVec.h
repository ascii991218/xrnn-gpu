#ifndef _GOPVEC_
#define _GOPVEC_

#include <vector>

class GOp {
    public:
        bool need;
        const char *name;
        const char *ty;
        const char *str;
        const char *get;
        const char *def; //default
        const char *how; //explain whether it's set by default
        GOp(const char *n, const char *t, const char* s, const char* de, bool ne = true);
};

class GOpVec {
    private:
        int size;
        std::vector<GOp*> *ops;
    public:
        GOpVec();  
	~GOpVec(); 
        void insert(GOp*);
        std::vector<GOp*>::iterator begin();
        std::vector<GOp*>::iterator end();
        const char* findOp(const char *name);
        void sweepArgs(int argc, char** argv);
};

#endif
