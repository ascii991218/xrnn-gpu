#include "GVocab.h"
#include <string.h>
#include <string>

GWord::GWord(const char *w, int size) {
    word = new char[strlen(w) + 1];
    strcpy(word, w);
    cuda_safecall(cudaMalloc((void **)&vec, size * sizeof(WEIGHT_TYPE)));
	randGen->set(vec, 1);
}

GWord::~GWord()
{
	delete[] word;
	cuda_safecall(cudaFree(vec));
}

void GVocab::readWord(char* word, FILE *fin) {
    int a = 0, ch;
    ch = fgetc(fin);
    while (ch != -1) {
        //if (ch == 13) continue;
        if ((ch == ' ') || (ch == '\t') || (ch == '\n')) {
            if (a > 0) {
                if (ch == '\n') ungetc(ch, fin);
                break;
            } else
            if (ch == '\n') {
                strcpy(word, (char *)"</s>");
	//printf("readWord :_%s\n", word); //debug
   				return;
            } else {
				ch = fgetc(fin);
				continue;
			}
        }
        word[a] = ch;
        a++;
        if (a >= MAX_WORD_LENGTH - 1) { //Truncate too long words
			gwarn("GVocab::readWord Meet a very long word");
			a--;   		
		}
        ch = fgetc(fin);
    }
    word[a] = 0;
	//printf("readWord :_%s\n", word); //debug
}
int GVocab::learnVocabFromFile(const char *fn) {
    FILE *file = fopen(fn, "r");
    if (file == NULL) {
        gerr("Train text file %s not found.\n", fn);
    }
    int words = 0;
    int sentences = 0;
    char buffer[MAX_WORD_LENGTH];
    readWord(buffer, file);
    while (strlen(buffer) != 0) {
        if (!strcmp(buffer, "</s>")) sentences++; else words++;	
		addWord(buffer);
		readWord(buffer, file);
    }
    fclose(file);
    glog(1, "Learned Vocab from file %s, words : %d, sentences : %d, Vocab size now : %d\n", fn, words, sentences, this->size());
    return words;
}
GVocab::GVocab(int vs) {
    vec_size = vs;
    words = new std::vector<GWord*>();
    hash_table = new int[VOCAB_HASH_SIZE];
    for (int i = 0;i < VOCAB_HASH_SIZE;i++)
        hash_table[i] = -1;
    addWord("</s>");
    addWord("<unk>");
}

GVocab::~GVocab()
{
	delete words;
	delete[] hash_table;
}

int GVocab::getHash(const char *w) {
    long long hash = 0;
    for (int i = 0;i < strlen(w);i++) {
        hash = (hash * 54059 + w[i] + 256) % VOCAB_HASH_SIZE; //w[i] could be negative
    }
    return (int)hash;
}
int GVocab::getUnkInd() {
	int k = findWord("<unk>");
	if (k < 0)
		gerr("GVocab::getUnkInd did not find <unk> in the vocab");
	return k;
}
void GVocab::printVocab(const char *f) {
    FILE *file = fopen(f, "w");
    for (int i = 0;i < words->size();i++)
        fprintf(file, "%s\n", words->at(i)->word);
    fclose(file);
}
int GVocab::findWord(const char *w) {  
    int h, beg;
    h = beg = getHash(w);
    while (hash_table[h] != -1) {
        if (!strcmp(words->at(hash_table[h])->word, w)) return hash_table[h];
        h = (h + 1) % VOCAB_HASH_SIZE;
        if (h == beg) 
            break;
    }
    return -1;
}
void GVocab::addWordToHash(const char *w, int index) {
    int h = getHash(w);
    while (hash_table[h] != -1) {
        if (hash_table[h] == index)
            return;
        h = (h + 1) % VOCAB_HASH_SIZE;
    }
    hash_table[h] = index;
}
int GVocab::addWord(const char *w) {
	int k = findWord(w);
	if (k != -1) {
        return k;
    }
    words->push_back(new GWord(w, vec_size));
    addWordToHash(w, words->size() - 1);
	return words->size() - 1;
}
int GVocab::addWV(const char *w, const WEIGHT_TYPE *v) {
	int k = findWord(w);
	if (k == -1) 
		k = addWord(w);
	WEIGHT_TYPE *vec = words->at(k)->vec;
	cuda_safecall(cudaMemcpy(vec, v, sizeof(WEIGHT_TYPE) * vec_size, cudaMemcpyHostToDevice));
	return k;
}
int GVocab::getWVFromFile(FILE *file) {
	int lines, size;
	char buffer[MAX_WORD_LENGTH];
	WEIGHT_TYPE buffer_v[vec_size];
	fscanf(file, "%s %d %d", buffer, &lines, &size); //VOCAB vocab_size hidden_size
	if (size != vec_size) 
		gerr("GVocab::getWVFromFile vec_size(%d) mismatch.", size);
	for (int i = 0;i < lines;i++) {
		fscanf(file, "%s", buffer);
		for (int j = 0;j < vec_size;j++)
			fscanf(file, WEIGHT_READ, buffer_v + j);
		addWV(buffer, buffer_v);
	}
	return words->size();
}
void GVocab::rehash() { 
    for (int i = 0;i < VOCAB_HASH_SIZE;i++)
        hash_table[i] = -1;
    for (int i = 0;i < words->size();i++)
        addWordToHash(words->at(i)->word, i);
}
int GVocab::size() {
	return words->size();
}
void GVocab::loadVocabFile(const char *file) {
    FILE *fin = fopen(file, "r");
    char buffer[MAX_WORD_LENGTH];
    addWord("</s>");
    readWord(buffer, fin);
    while (strlen(buffer) != 0) {
        if (strcmp(buffer, "</s>")) {
            addWord(buffer);
        }
        readWord(buffer, fin);
    }
    fclose(fin);
}
int GVocab::getVecSize() {
    return vec_size;
}
GWord *GVocab::getWord(int index) {
	if (index < 0 || index >= size()) {
		gerr("GVocab::getWord Index out of range.\n");
	}
	return words->at(index);
}	
